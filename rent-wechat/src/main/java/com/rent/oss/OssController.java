package com.rent.oss;


import java.util.Map;

import com.rent.annotation.NoAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author lihaoyang
 * @date 2021/1/22
 */
@RestController
public class OssController {

    @Autowired
    OssService ossService;

    /**
     * 获取oss 配置
     * @param
     * @return
     */
    @NoAuth
    @GetMapping("oss/config")
    public Map<String, String> getOSSConfig(){
        return ossService.getOssConfig();
    }
}
