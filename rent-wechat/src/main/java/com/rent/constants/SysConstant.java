package com.rent.constants;

/**
 * @author lihaoyang
 * @date 2020/11/12
 */
public class SysConstant {


    public static final String TOKEN = "X-ACCESS-TOKEN";

    public static final String DICT_SPLIT = ",";

    //字典后后缀
    public static final String DICT_LABEL_SUFFIX ="Text";

    /**
     * 字典信息缓存
     */
    public static final String SYS_DICT_CACHE = "sys:dict";

    /**
     * 测试缓存key
     */
    public static final String TEST_DEMO_CACHE = "test:demo";

    /**app位置信息前缀key**/
    public static final String HOUSE_GEO_PREFIX = "house:geo:pos:";

}
