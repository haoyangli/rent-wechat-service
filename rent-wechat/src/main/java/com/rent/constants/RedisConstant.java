package com.rent.constants;

/**
 * @author lihaoyang
 * @date 2021/1/24
 */
public interface RedisConstant {

    /** 登录用户Token令牌缓存KEY前缀 */
    public static final String PREFIX_USER_TOKEN  = "user:token:";
    /** Token缓存时间：3600秒即一小时 */
    public static final int  TOKEN_EXPIRE_TIME  = 3600;

    /**
     * 缓存用户信息
     */
    public static final String SYS_USERS_CACHE = "login:user:";

}
