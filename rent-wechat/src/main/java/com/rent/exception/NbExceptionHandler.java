package com.rent.exception;

import com.rent.beans.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理
 *
 * @author lihaoyang
 * @date 2021/1/22
 */
@Slf4j
@RestControllerAdvice
public class NbExceptionHandler {
    /**
     * 搞个自定义的业务异常
     * @param e
     * @return
     */
    @ExceptionHandler(BusinessException.class)
    public Result<?> handleException(BusinessException e) {
        log.error(e.getMessage(), e);
        return Result.error(e.getCode(), e.getErrorMsg());
    }

    @ExceptionHandler(NbBaseException.class)
    public Result<?> handlerNbBaseException(NbBaseException e) {
        log.error(e.getMessage(), e);
        return Result.error(e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public Result<?> handleException(Exception e) {
        log.error(e.getMessage(), e);
        //return Result.error("服务器异常，"+e.getMessage());
        return Result.error("服务器异常");
    }

}
