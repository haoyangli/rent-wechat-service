package com.rent.exception;

/**
 * @author lihaoyang
 * @date 2021/1/22
 */
public class NbBaseException extends RuntimeException{


    public NbBaseException(String message) {
        super(message);
    }
}
