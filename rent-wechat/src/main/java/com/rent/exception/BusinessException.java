package com.rent.exception;

/**
 * 自定义业务异常
 */
public class BusinessException extends RuntimeException {
    private Integer code;
    private String errorMsg;

    public BusinessException(Integer code, String errorMsg) {
        super(errorMsg);
        this.code = code;
        this.errorMsg = errorMsg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
