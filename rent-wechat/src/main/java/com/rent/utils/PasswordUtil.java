package com.rent.utils;

import cn.hutool.crypto.SecureUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author lihaoyang
 * @date 2021/1/23
 */
public class PasswordUtil {

    /**
     * 校验密码
     * @param password1 明文密码
     * @param password2 数据库密文密码
     * @param salt 盐
     * @return
     */
    public static boolean checkPassword(String password1,String salt,String password2){
        return StringUtils.equals(encryptPwd(password1,salt),password2);
    }


    public static String encryptPwd(String password,String salt){
        if(StringUtils.isBlank(password)){
            throw new IllegalArgumentException("密码不能为空");
        }
        if(StringUtils.isBlank(salt)){
            throw new IllegalArgumentException("盐不能为空");
        }
        return SecureUtil.sha256(password+salt);
    }
    public static String getSalt(){
        return RandomStringUtils.randomAlphanumeric(8);
    }


    public static void main(String[] args) {
//        String salt = getSalt();
//        System.err.println(salt);
        String pwd = encryptPwd("123456","lMOHp0qN");
        System.err.println(pwd);
        System.err.println(pwd.length());


    }


}
