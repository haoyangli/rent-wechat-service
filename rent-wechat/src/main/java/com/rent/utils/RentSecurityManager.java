package com.rent.utils;

import com.rent.beans.vo.LoginUser;

/**
 * 保存用户登录信息
 * @author lihaoyang
 * @date 2021/6/20
 */
public class RentSecurityManager {
    private static ThreadLocal<LoginUser> userThreadLocal = new ThreadLocal<>();


    public static void setLoginUser(LoginUser loginUser){
        userThreadLocal.set(loginUser);
    }

    public static LoginUser getLoginUser(){
        return userThreadLocal.get();
    }

    public static Integer getUserId(){
        return userThreadLocal.get().getUserId();
    }

    public static String getOpenId(){
        return userThreadLocal.get().getOpenId();
    }

    public static void removeLoginUser(){
        userThreadLocal.remove();
    }
}
