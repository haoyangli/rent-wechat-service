package com.rent.service;

import com.rent.entity.PublishRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2021-06-05
 */
public interface PublishRecordService extends IService<PublishRecord> {
   int deleteByHouseId(Integer houseId);
}
