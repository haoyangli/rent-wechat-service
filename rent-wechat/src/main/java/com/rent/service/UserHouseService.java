package com.rent.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rent.beans.resp.HouseListResp;
import com.rent.entity.UserHouse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2021-06-05
 */
public interface UserHouseService extends IService<UserHouse> {

    /**
     * 关注房源
     *
     * @param userId
     * @param houseId
     * @return
     */
    boolean careHouse(Integer userId,Boolean care, Integer houseId);

    /**
     * 我的关注
     *
     * @param page
     * @param userId
     * @return
     */
    IPage<HouseListResp> findPageByUserId(Page<?> page, Integer userId);


    /**
     * 是否关注房源
     *
     * @param userId
     * @param houseId
     * @return
     */
    boolean isCareHouse(Integer userId, Integer houseId);

    /**
     * 删除
     * @param houseId
     * @return
     */
    int deleteByHouseId(Integer houseId);
}
