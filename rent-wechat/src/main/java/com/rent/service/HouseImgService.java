package com.rent.service;

import com.rent.entity.HouseImg;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2021-06-05
 */
public interface HouseImgService extends IService<HouseImg> {


    List<HouseImg> getListByHouseId(Integer houseId);
}
