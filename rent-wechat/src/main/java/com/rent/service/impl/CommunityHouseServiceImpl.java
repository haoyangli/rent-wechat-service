package com.rent.service.impl;

import com.rent.entity.CommunityHouse;
import com.rent.mapper.CommunityHouseMapper;
import com.rent.service.CommunityHouseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2021-06-05
 */
@Service
public class CommunityHouseServiceImpl extends ServiceImpl<CommunityHouseMapper, CommunityHouse> implements CommunityHouseService {

}
