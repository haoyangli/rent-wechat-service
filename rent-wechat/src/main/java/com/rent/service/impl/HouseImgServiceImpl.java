package com.rent.service.impl;

import com.rent.entity.HouseImg;
import com.rent.mapper.HouseImgMapper;
import com.rent.service.HouseImgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2021-06-05
 */
@Service
public class HouseImgServiceImpl extends ServiceImpl<HouseImgMapper, HouseImg> implements HouseImgService {

    @Autowired
    HouseImgMapper houseImgMapper;

    @Override
    public List<HouseImg> getListByHouseId(Integer houseId) {
        return houseImgMapper.getListByHouseId(houseId);
    }
}
