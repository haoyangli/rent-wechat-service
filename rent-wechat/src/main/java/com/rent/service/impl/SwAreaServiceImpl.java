package com.rent.service.impl;

import com.rent.beans.resp.IdNameResp;
import com.rent.entity.SwArea;
import com.rent.mapper.SwAreaMapper;
import com.rent.service.SwAreaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2021-01-20
 */
@Slf4j
@Service
public class SwAreaServiceImpl extends ServiceImpl<SwAreaMapper, SwArea> implements SwAreaService {

    @Autowired
    private SwAreaMapper swAreaMapper;

    @Override
    public List<IdNameResp> findIdNameListByParentId(String parentId) {
        return swAreaMapper.selectIdNameListByParentId(parentId);
    }

    @Override
    public String testLog(String name, Integer id) {
        log.info("Service 自己打印的日志：name{}",name);
        return "XXOO";
    }
}
