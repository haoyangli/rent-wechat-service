package com.rent.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rent.entity.PublishRecord;
import com.rent.mapper.PublishRecordMapper;
import com.rent.service.PublishRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author
 * @since 2021-06-05
 */
@Service
public class PublishRecordServiceImpl extends ServiceImpl<PublishRecordMapper, PublishRecord> implements PublishRecordService {
    @Autowired
    private PublishRecordMapper recordMapper;

    @Override
    public int deleteByHouseId(Integer houseId) {
        return recordMapper.updateHouseDeleteStatus(houseId);
    }
}
