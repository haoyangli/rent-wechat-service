package com.rent.service;

import com.rent.entity.CommunityHouse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2021-06-05
 */
public interface CommunityHouseService extends IService<CommunityHouse> {

}
