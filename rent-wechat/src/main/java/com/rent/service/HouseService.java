package com.rent.service;

import com.rent.beans.request.PublishRequest;
import com.rent.beans.resp.HouseListResp;
import com.rent.beans.resp.HouseRegionResp;
import com.rent.entity.House;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2021-06-05
 */
public interface HouseService extends IService<House> {

    /**
     * 发布
     * @param publishRequest
     * @param userId
     */
    void publishHouse(PublishRequest publishRequest, Integer userId);

    List<HouseListResp> nearbyHouse();

    List<House> findByHouseIdList(List<Integer> houseIds);

    List<HouseRegionResp> getHouseRegionList();
    List<House> getListByCityAndDistinct(String city,String district);
}
