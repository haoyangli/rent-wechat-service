package com.rent.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rent.entity.WechatUser;
import com.rent.model.LoginResultModel;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author
 * @since 2021-06-05
 */
public interface WechatUserService extends IService<WechatUser> {
    /**
     * 微信登录
     */
    LoginResultModel wechatLogin(String code, String appid, String secret);


    WechatUser getByOpenId(String openId);

}
