package com.rent.service;

import com.rent.beans.resp.IdNameResp;
import com.rent.entity.SwArea;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2021-01-20
 */
public interface SwAreaService extends IService<SwArea> {


    List<IdNameResp> findIdNameListByParentId(String parentId);

    String testLog(String name,Integer id);
}
