package com.rent.service;

import com.rent.model.JsCode2SessionModel;

/**
 * 和微信交互相关的service
 */
public interface WeChatService {
    JsCode2SessionModel getWechatMsgByCode(String code,String appid,String secret);
}
