package com.rent.enums;

public enum UserStatusEnum {

    //用户状态0-禁用；1-启用
    LOCK(0,"禁用"),
    NORMAL(1,"启用")
    ;


    private final Integer code;
    private final String message;

    UserStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
