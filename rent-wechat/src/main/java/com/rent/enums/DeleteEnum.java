package com.rent.enums;

/**
 * 删除标识
 */
public enum DeleteEnum {

    NO(0,"未删除"),
    YES(1,"已删除");

    private final Integer code;
    private final String message;

    DeleteEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
