package com.rent.enums;

/**
 * 错误码枚举
 * @author lihaoyang
 * @date 2021/1/22
 */
public enum ErrorEnum {


    SUCCESS(200,"请求成功"),
    ERROR(500,"请求失败"),

    NO_AUTH(403,"没有权限"),


    TOKEN_NULL(10010,"token为空"),
    TOKEN_PARSE_ERROR(10011,"token解析失败"),
    TOKEN_EXPIRED(10012,"token失效请重新登录"),

    UNAME_PWD_ERROR(10021,"用户名或密码错误"),
    USER_LOCK(100022,"账号禁用"),

    USER_NOT_EXIST(10023,"用户不存在"),
    USER_LOCKED(10024,"用户已锁定请联系管理员"),
    ;


    private final Integer code;
    private final String message;

    ErrorEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
