package com.rent.enums;

/**
 * 付款方式(1-月付;2-季付;3-半年付;4-年付)
 *
 * @author lihaoyang
 * @date 2021/6/7
 */
public enum PayTypeEnum {

    MON(1, "月付"),
    QUARTER(2, "季付"),
    HALF_YEAR(3, "半年付"),
    YEAR(4, "年付")

    ;


    private final Integer code;
    private final String message;

    PayTypeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static String getMsg(Integer code){
        PayTypeEnum[] values = PayTypeEnum.values();
        for(PayTypeEnum en : values){
            if(code.equals(en.code)){
                return en.getMessage();
            }
        }
        return "";
    }

}
