package com.rent.enums;

/**
 * 发布状态(0-未发布;1-已发布)
 */
public enum PublishStatusEnum {

    NOT_PUB(0,"未发布"),
    PUB(1,"已发布")
    ;


    private final Integer code;
    private final String message;

    PublishStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
