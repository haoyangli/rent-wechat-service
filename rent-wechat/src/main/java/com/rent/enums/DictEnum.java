package com.rent.enums;

public enum DictEnum {

    POST_TAG("post_tag","帖子标签"),
    ;

    private final String dictCode;
    private final String dictName;

    DictEnum(String dictCode, String dictName) {
        this.dictCode = dictCode;
        this.dictName = dictName;
    }

    public String getDictCode() {
        return dictCode;
    }

    public String getDictName() {
        return dictName;
    }
}
