package com.rent.enums;

/**
 * 户型(0-单间;1-一室;2-两室;3-三室;4-四室)
 *
 * @author lihaoyang
 * @date 2021/6/7
 */
public enum HouseTypeEnum {

    DAN_JIAN(0, "单间"),
    ONE(1, "一室"),
    TWO(2, "两室"),
    THREE(3, "三室"),
    FOUR(4, "四室");


    private final Integer code;
    private final String message;

    HouseTypeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static String getMsg(Integer code){
        HouseTypeEnum[] values = HouseTypeEnum.values();
        for(HouseTypeEnum en : values){
            if(code.equals(en.code)){
                return en.getMessage();
            }
        }
        return "";
    }

}
