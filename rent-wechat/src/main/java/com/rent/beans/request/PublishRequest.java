package com.rent.beans.request;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author lihaoyang
 * @date 2021/6/6
 */
@Data
public class PublishRequest {
    /**
     * 维度
     */
    private BigDecimal lat;

    /**
     * 经度
     */
    private BigDecimal lng;

    /**
     * 房子标题
     */
    @NotBlank(message = "标题不能为空")
    private String title;

    /**
     * 腾讯地图搜索地点的地址，如:"北京市丰台区莲花池东路",
     */
    private String address;
    /**
     *     腾讯地图返回省市区
     */
    private String province;
    private String city;
    private String district;



    /**
     * 所在楼层
     */
    private Integer floor;

    /**
     * 总楼层
     */
    private Integer totalFloor;
    /**
     * 房屋大小(平方米)
     */
    @Min(value = 1, message = "请填写房屋面积")
    private Integer size;

    /**
     * 租金(元/月)
     */
    @Min(value = 1, message = "请填写租金")
    private Integer price;

    /**
     * 房屋配置字典id
     */
    private List<Integer> houseConfigIdList;

    /**
     * 付款方式(1-月付;2-季付;3-半年付;4-年付)
     */
    private Integer payType;

    private Integer payTypeIndex;

    /**
     * 户型(0-单间;1-一室;2-两室;3-三室;4-四室)
     */
    private Integer houseType;

    private Integer houseTypeIndex;
    /**
     * 描述
     */
    private String description;


    /**
     * 联系人
     */
    private String linkman;

    /**
     * 联系人手机
     */
    private String linkMobile;
    /**
     * 联系人手机
     */
    private String linkMobile2;


    /**
     * 联系人微信
     */
    private String linkWechat;

    /**
     * 封面图
     */
    private String coverImg;

    /**
     * 图片
     */
    private List<String> houseImgs;

}
