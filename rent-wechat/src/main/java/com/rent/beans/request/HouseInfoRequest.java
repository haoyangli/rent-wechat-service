package com.rent.beans.request;

import lombok.Data;

/**
 * 关注房源
 * @author lihaoyang
 * @date 2021/6/20
 */
@Data
public class HouseInfoRequest {

    private Integer houseId;
    private String city;
    private String district;
    //关注或者取消关注
    private Boolean care;
}
