package com.rent.beans.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 登录
 * @author lihaoyang
 * @date 2021/1/23
 */
@Data
public class LoginRequest {

    @NotBlank
    private String nickname;

    private String sex;

    private String headImg;

    private String openId;
}
