package com.rent.beans.resp;

/**
 * 房屋区域信息
 */
public class HouseRegionResp {
    private String city;
    private String district;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
