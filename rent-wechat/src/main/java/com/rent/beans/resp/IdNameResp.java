package com.rent.beans.resp;

import lombok.Data;

/**
 * @author lihaoyang
 * @date 2021/1/21
 */
@Data
public class IdNameResp {
    private Integer id;
    private String name;
}
