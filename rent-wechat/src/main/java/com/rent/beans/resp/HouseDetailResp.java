package com.rent.beans.resp;

import com.rent.entity.House;
import lombok.Data;

import java.util.List;

/**
 * @author lihaoyang
 * @date 2021/6/7
 */
@Data
public class HouseDetailResp extends House {

    private boolean careFlag;

    private String houseTypeText;

    private String payTypeText;

    private List<String> imgList;
}
