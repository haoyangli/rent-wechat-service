package com.rent.beans.resp;

import com.rent.entity.House;
import com.rent.enums.HouseTypeEnum;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author lihaoyang
 * @date 2021/6/6
 */
@Data
public class HouseListResp {

    private Integer id;
    private String img;
    private String title;
    private Integer price;
    private Integer size;
    private String houseType;
    private String address;
    private BigDecimal latitude;
    private BigDecimal longitude;

    private String distance;

    public HouseListResp(){}

    public HouseListResp(House house){
        this.id = house.getId();
        this.title = house.getTitle();
        this.address = house.getAddress();
        this.img = house.getCoverImg();
        this.price = house.getPrice();
        this.latitude = house.getLat();
        this.longitude = house.getLng();
        this.houseType = HouseTypeEnum.getMsg(house.getHouseType());
        this.size = house.getSize();

    }



}
