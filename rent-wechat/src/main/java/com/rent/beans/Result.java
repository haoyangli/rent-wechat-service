package com.rent.beans;

import java.io.Serializable;

import com.rent.enums.ErrorEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@ApiModel(value="接口返回对象", description="接口返回对象")
public class Result<T> implements Serializable {

	private static final long serialVersionUID = 1L;



	/**
	 * 返回处理消息
	 */
	@ApiModelProperty(value = "返回处理消息")
	private String message = ErrorEnum.SUCCESS.getMessage();

	/**
	 * 返回代码
	 */
	@ApiModelProperty(value = "返回代码")
	private Integer code = ErrorEnum.SUCCESS.getCode();
	
	/**
	 * 返回数据对象 data
	 */
	@ApiModelProperty(value = "返回数据对象")
	private T result;
	
	/**
	 * 时间戳
	 */
	@ApiModelProperty(value = "时间戳")
	private long timestamp = System.currentTimeMillis();

	public Result() {
		
	}
	
	public Result<T> success(String message) {
		this.message = message;
		this.code = ErrorEnum.SUCCESS.getCode();
		return this;
	}
	

	


	
	public static<T> Result<T> OK() {
		Result<T> r = new Result<T>();
		r.setCode(ErrorEnum.SUCCESS.getCode());
		r.setMessage(ErrorEnum.SUCCESS.getMessage());
		return r;
	}
	
	public static<T> Result<T> OK(T data) {
		Result<T> r = new Result<T>();
		r.setCode(ErrorEnum.SUCCESS.getCode());
		r.setMessage(ErrorEnum.SUCCESS.getMessage());
		r.setResult(data);
		return r;
	}


	public static Result<Object> error(ErrorEnum errorEnum) {
		return error(errorEnum.getCode(), errorEnum.getMessage());
	}

	
	public static Result<Object> error(String msg) {
		return error(ErrorEnum.ERROR.getCode(), msg);
	}
	
	public static Result<Object> error(int code, String msg) {
		Result<Object> r = new Result<Object>();
		r.setCode(code);
		r.setMessage(msg);
		return r;
	}

	public Result<T> error500(String message) {
		this.message = message;
		this.code = ErrorEnum.ERROR.getCode();
		return this;
	}
	/**
	 * 无权限访问返回结果
	 */
	public static Result<Object> noAuth(String msg) {
		return error(ErrorEnum.NO_AUTH.getCode(), msg);
	}
}