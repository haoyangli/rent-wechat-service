package com.rent.beans.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rent.entity.WechatUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author lihaoyang
 * @date 2021/1/24
 */
@AllArgsConstructor
@Data
public class LoginUser {

    private Integer userId;
    private String openId;
    private String mobile;
    private String nickname;
    private String headImg;
    private Integer sex;
    private Integer status;

    public LoginUser(){}

    public LoginUser(WechatUser wechatUser){
        this.userId = wechatUser.getId();
        this.openId = wechatUser.getOpenId();
        this.mobile = wechatUser.getMobile();
        this.nickname = wechatUser.getNickname();
        this.sex = wechatUser.getSex();
        this.status = wechatUser.getStatus();
    }

}
