package com.rent.beans.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lihaoyang
 * @date 2021/6/11
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class NearbyGeoDTO {

    private Double distince;

    private Integer houseId;
}
