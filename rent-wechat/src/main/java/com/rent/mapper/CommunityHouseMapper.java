package com.rent.mapper;

import com.rent.entity.CommunityHouse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-06-05
 */
public interface CommunityHouseMapper extends BaseMapper<CommunityHouse> {

}
