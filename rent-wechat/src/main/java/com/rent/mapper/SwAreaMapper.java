package com.rent.mapper;

import com.rent.beans.resp.IdNameResp;
import com.rent.entity.SwArea;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-01-20
 */
public interface SwAreaMapper extends BaseMapper<SwArea> {


    List<IdNameResp> selectIdNameListByParentId(@Param("parentId") String parentId);
}
