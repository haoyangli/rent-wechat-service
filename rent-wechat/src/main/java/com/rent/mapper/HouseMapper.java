package com.rent.mapper;

import com.rent.beans.resp.HouseRegionResp;
import com.rent.entity.House;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-06-05
 */
public interface HouseMapper extends BaseMapper<House> {

    List<House> findByHouseIdList(@Param("houseIds") List<Integer> houseIds);

    @Select("select  city,district from house where city is not null   group by  city,district")
    List<HouseRegionResp> getHouseRegionList();
}
