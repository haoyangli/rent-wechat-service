package com.rent.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rent.entity.PublishRecord;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author
 * @since 2021-06-05
 */
public interface PublishRecordMapper extends BaseMapper<PublishRecord> {
    @Update("update publish_record set is_del=1 where house_id=#{houseId}")
    int updateHouseDeleteStatus(@Param("houseId") Integer houseId);
}
