package com.rent.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rent.beans.resp.HouseListResp;
import com.rent.entity.UserHouse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-06-05
 */
public interface UserHouseMapper extends BaseMapper<UserHouse> {

    /**
     * 我的关注
     *
     * @param page
     * @param userId
     * @return
     */
    IPage<HouseListResp> selectPageByUserId(Page<?> page, @Param("userId") Integer userId);

    @Update("update user_house set is_del=1 where house_id=#{houseId}")
    int updateDeleteStatusByHouseId(@Param("houseId") Integer houseId);
}
