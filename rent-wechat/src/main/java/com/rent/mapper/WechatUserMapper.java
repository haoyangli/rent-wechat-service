package com.rent.mapper;

import com.rent.entity.WechatUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-06-05
 */
public interface WechatUserMapper extends BaseMapper<WechatUser> {


    WechatUser selectByOpenId(@Param("openId") String openId);
}
