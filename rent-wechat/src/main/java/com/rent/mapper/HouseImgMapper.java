package com.rent.mapper;

import com.rent.entity.HouseImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2021-06-05
 */
public interface HouseImgMapper extends BaseMapper<HouseImg> {

    List<HouseImg> getListByHouseId(@Param("houseId") Integer houseId);
}
