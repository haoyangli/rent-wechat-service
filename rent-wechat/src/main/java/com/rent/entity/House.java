package com.rent.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2021-06-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("house")
public class House implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 房子标题
     */
    private String title;

    /**
     * 封面图
     */
    private String coverImg;

    private String address;

    /**
     * 租金(元/月)
     */
    private Integer price;

    /**
     * 房屋大小(平方米)
     */
    private Integer size;

    /**
     * 户型(0-单间;1-一室;2-两室;3-三室;4-四室)
     */
    private Integer houseType;

    /**
     * 所在楼层
     */
    private Integer floor;

    /**
     * 总楼层
     */
    private Integer totalFloor;

    /**
     * 付款方式(1-月付;2-季付;3-半年付;4-年付)
     */
    private Integer payType;

    /**
     *     腾讯地图返回省市区
     */
    private String province;
    private String city;
    private String district;

    /**
     * 配套设施
     */
    private String config;

    /**
     * 描述
     */
    private String description;

    /**
     * 维度
     */
    private BigDecimal lat;

    /**
     * 经度
     */
    private BigDecimal lng;

    /**
     * 联系人
     */
    private String linkman;

    /**
     * 联系人手机
     */
    private String linkMobile;

    /**
     * 联系人手机
     */
    private String linkMobile2;

    /**
     * 联系人微信
     */
    private String linkWechat;

    /**
     * 发布状态(0-未发布;1-已发布)
     */
    private Integer publishStatus;

    /**
     * 查看次数
     */
    private Integer viewNum;

    /**
     * 创建人id
     */
    private Integer createUserId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;


    /**
     * 是否删除(0-否;1-是)
     */
    private Integer isDel;


}
