package com.rent.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2021-01-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sw_area")
public class SwArea implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String fullName;

    private Integer parentId;

    private String shortName;

    private Integer level;

    private String cityCode;

    private String zipCode;

    private String mergerName;

    private BigDecimal lng;

    private BigDecimal lat;

    private String pinyin;

    private Date createTime;

    private Date updateTime;


}
