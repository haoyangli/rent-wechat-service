package com.rent.controller;

import com.rent.beans.Result;
import com.rent.service.HouseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhuguohui
 * @date 2021/6/26
 * 房屋地图相关的controller
 */
@Slf4j
@RequestMapping("/housemap")
@RestController
public class HouseMapController {
    @Autowired
    private HouseService houseService;

    /**
     * 查询区域信息
     */
    @PostMapping("/getRegionList")
    public Result getRegionList() {
        return Result.OK(this.houseService.getHouseRegionList());
    }
}
