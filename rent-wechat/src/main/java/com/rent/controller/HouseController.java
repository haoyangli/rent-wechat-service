package com.rent.controller;


import cn.hutool.core.bean.BeanUtil;
import com.rent.annotation.NoAuth;
import com.rent.beans.Result;
import com.rent.beans.request.HouseInfoRequest;
import com.rent.beans.request.PublishRequest;
import com.rent.beans.resp.HouseDetailResp;
import com.rent.beans.resp.HouseListResp;
import com.rent.entity.House;
import com.rent.entity.HouseImg;
import com.rent.enums.HouseTypeEnum;
import com.rent.enums.PayTypeEnum;
import com.rent.service.HouseImgService;
import com.rent.service.HouseService;
import com.rent.service.UserHouseService;
import com.rent.utils.RentSecurityManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author
 * @since 2021-06-05
 */
@Api(tags = "房源")
@Slf4j
@RestController
@RequestMapping("/house")
public class HouseController {

    @Autowired
    HouseService houseService;

    @Autowired
    UserHouseService userHouseService;

    @Autowired
    HouseImgService houseImgService;


    @ApiOperation("发布房源")
    @PostMapping("publish")
    public Result publish(@RequestBody PublishRequest publishRequest) {
        Integer userId = RentSecurityManager.getUserId();
        houseService.publishHouse(publishRequest, userId);
        return Result.OK();
    }

    @NoAuth
    @ApiOperation("房源列表")
    @GetMapping("list")
    public Result list() {
        List<House> list = houseService.list();
        List<HouseListResp> resultList = new ArrayList<>();
        for (House house : list) {
            resultList.add(new HouseListResp(house));
        }
        return Result.OK(resultList);
    }

    @ApiOperation("房源列表-post")
    @PostMapping("getListByCityAndDistinct")
    public Result getListByCityAndDistinct(@RequestBody HouseInfoRequest houseInfoRequest) {
        List<House> list = houseService.getListByCityAndDistinct(houseInfoRequest.getCity(), houseInfoRequest.getDistrict());
        List<HouseListResp> resultList = new ArrayList<>();
        for (House house : list) {
            resultList.add(new HouseListResp(house));
        }
        return Result.OK(resultList);
    }

    @ApiOperation("房源详情")
    @GetMapping("detail")
    public Result detail(@RequestParam Integer houseId) {
        //
        House house = houseService.getById(houseId);
        List<HouseImg> houseImgs = houseImgService.getListByHouseId(houseId);
        List<String> imgList = houseImgs.stream().map(HouseImg::getImgUrl).collect(Collectors.toList());
        HouseDetailResp detailResp = new HouseDetailResp();
        BeanUtil.copyProperties(house, detailResp);
        detailResp.setHouseTypeText(HouseTypeEnum.getMsg(house.getHouseType()));
        detailResp.setPayTypeText(PayTypeEnum.getMsg(house.getPayType()));
        detailResp.setImgList(imgList);
        //是否关注该房源
        Integer userId = RentSecurityManager.getUserId();
        log.error("错误标记2",userId);
        boolean careHouse = userHouseService.isCareHouse(userId, houseId);
        detailResp.setCareFlag(careHouse);
        return Result.OK(detailResp);
    }


    @ApiOperation("刷新时间")
    @PostMapping("refreshTime")
    public Result refreshTime(@RequestBody HouseInfoRequest houseInfoRequest) {
        House house = new House();
        house.setId(houseInfoRequest.getHouseId());
        house.setUpdateTime(new Date());
        houseService.updateById(house);
        return Result.OK();
    }


}

