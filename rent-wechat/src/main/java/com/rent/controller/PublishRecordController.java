package com.rent.controller;


import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.rent.beans.Result;
import com.rent.beans.request.HouseInfoRequest;
import com.rent.beans.resp.HouseListResp;
import com.rent.entity.House;
import com.rent.entity.PublishRecord;
import com.rent.enums.DeleteEnum;
import com.rent.service.HouseService;
import com.rent.service.PublishRecordService;
import com.rent.service.UserHouseService;
import com.rent.utils.RentSecurityManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author
 * @since 2021-06-05
 */
@Api(tags = "我的发布")
@Slf4j
@RestController
@RequestMapping("/publishRecord")
public class PublishRecordController {

    @Autowired
    PublishRecordService publishRecordService;

    @Autowired
    HouseService houseService;

    @Autowired
    UserHouseService userHouseService;

    /**
     * 我的发布
     *
     * @return
     */
    @ApiOperation("我发布的房源列表")
    @GetMapping("me")
    public Result myPublishList() {
        Integer userId = RentSecurityManager.getUserId();
        //
        LambdaQueryWrapper<PublishRecord> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(PublishRecord::getPublishUserId, userId)
                .eq(PublishRecord::getIsDel, DeleteEnum.NO.getCode());
        List<PublishRecord> publishRecords = publishRecordService.list(queryWrapper);
        if (CollectionUtil.isEmpty(publishRecords)) {
            return Result.OK(Collections.emptyList());
        }
        List<Integer> houseIds = publishRecords.stream().map(PublishRecord::getHouseId).collect(Collectors.toList());
        List<House> houseList = houseService.listByIds(houseIds);
        List<HouseListResp> resultList = new ArrayList<>();
        for (House house : houseList) {
            HouseListResp resp = new HouseListResp(house);
            resultList.add(resp);
        }
        return Result.OK(resultList);
    }

    @ApiOperation("刷新")
    @PostMapping("refreshTime")
    public Result refreshTime(@RequestBody HouseInfoRequest houseInfoRequest) {
        House house = new House();
        house.setId(houseInfoRequest.getHouseId());
        house.setUpdateTime(new Date());
        houseService.updateById(house);
        return Result.OK();
    }

    @ApiOperation("删除")
    @PostMapping("delete")
    public Result delete(@RequestBody HouseInfoRequest houseInfoRequest) {
        House house = new House();
        house.setId(houseInfoRequest.getHouseId());
        house.setIsDel(DeleteEnum.YES.getCode());
        house.setUpdateTime(new Date());
        houseService.updateById(house);
        /**
         * 删除发布记录，发布记录有啥用
         */
        //删除发布记录
        publishRecordService.deleteByHouseId(house.getId());
        /**
         * 删除关注列表
         */
        this.userHouseService.deleteByHouseId(house.getId());
        return Result.OK();
    }
}

