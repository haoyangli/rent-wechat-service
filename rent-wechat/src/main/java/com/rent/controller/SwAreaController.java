package com.rent.controller;


import com.rent.annotation.NoAuth;
import com.rent.beans.Result;
import com.rent.beans.resp.IdNameResp;
import com.rent.service.SwAreaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2021-01-20
 */
@Slf4j
@RestController
@RequestMapping("/swArea")
public class SwAreaController {

    @Autowired
    SwAreaService swAreaService;

    @NoAuth
    @GetMapping("list")
    public Result list(@RequestParam String parentId){

        List<IdNameResp> list = swAreaService.findIdNameListByParentId(parentId);

        return Result.OK(list);
    }

    @NoAuth
    @GetMapping("log")
    public Result log(@RequestParam String parentId){
        log.info("自己打印的日志：parentId{}",parentId);
        String returnVal = swAreaService.testLog("骚欣", 100);
        return Result.OK(returnVal);
    }
}

