package com.rent.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rent.beans.Result;
import com.rent.beans.request.HouseInfoRequest;
import com.rent.beans.resp.HouseListResp;
import com.rent.service.UserHouseService;
import com.rent.utils.RentSecurityManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  我的关注前端控制器
 * </p>
 *
 * @author 
 * @since 2021-06-05
 */
@Slf4j
@RestController
@RequestMapping("/userHouse")
public class UserHouseController {

    @Autowired
    UserHouseService myCareService;

    @PostMapping("care")
    public Result careHouse(@RequestBody HouseInfoRequest careHouseRequest) {
        Integer userId = RentSecurityManager.getUserId();
        myCareService.careHouse(userId, careHouseRequest.getCare(),careHouseRequest.getHouseId());
        return Result.OK();
    }

    @GetMapping("careList")
    public Result careList(@RequestParam(defaultValue = "1") int pageNum,
                           @RequestParam(defaultValue = "20") int pageSize) {

        Integer userId = RentSecurityManager.getUserId();
        Page<HouseListResp> page = new Page(pageNum,pageSize);
        IPage<HouseListResp> pageList = myCareService.findPageByUserId(page, userId);
        return Result.OK(pageList);
    }
}

