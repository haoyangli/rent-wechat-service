package com.rent.model;

/**
 * https://api.weixin.qq.com/sns/jscode2session  响应结果
 */
public class JsCode2SessionModel {
    private static final Integer successCode = 0;
    //用户唯一标识
    private String openid;
    //会话密钥
    private String session_key;
    //用户在开放平台的唯一标识符，若当前小程序已绑定到微信开放平台帐号下会返回，详见 UnionID 机制说明
    private String unionid;
    //错误码
    /**
     * -1	系统繁忙，此时请开发者稍候再试
     * 0    请求成功
     * 40029 code 无效
     * 45011	频率限制，每个用户每分钟100次
     */
    private Integer errcode;
    //错误信息
    private String errmsg;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getSession_key() {
        return session_key;
    }

    public void setSession_key(String session_key) {
        this.session_key = session_key;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    public Integer getErrcode() {
        return errcode;
    }

    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    /**
     * 这个方法必须在调用腾讯接口之后再调用
     *
     * @return
     */
    public boolean isSuccess() {
        return successCode.equals(this.errcode);
    }

}
