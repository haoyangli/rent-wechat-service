package com.rent.model;

import lombok.Data;

/**
 * 登录的model
 */
@Data
public class LoginResultModel {
    private String openId;
    private String jwtToken;
    private WechatUserModel wechatUserModel;
}
