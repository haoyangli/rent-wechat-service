package com.rent;

import com.rent.constants.SysConstant;
import com.rent.utils.RedisGeoUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.*;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author lihaoyang
 * @date 2021/6/10
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class TTT {

    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    RedisGeoUtil redisGeoUtil;
    @Test
    public void testNearByXY(){


        Point point = new Point(116.308994,40.159142);
        Distance distance = new Distance(2000,Metrics.KILOMETERS);
        Circle circle = new Circle(point,distance);


        RedisGeoCommands.GeoRadiusCommandArgs args = RedisGeoCommands.GeoRadiusCommandArgs
                .newGeoRadiusArgs()
                .includeDistance()//距离
                .includeCoordinates()
                .sortAscending()//按距离排序
                .limit(5);

        redisGeoUtil.nearByXY("housepos",circle,args);

    }

    @Test
    public void add(){

        /**
         * 1	兴业家园	40.159142	116.308994
         * 2	欢乐家园	40.159951	116.308429
         * 3	庭好家园	40.159565	116.307872
         * 4	友谊家园	40.158141	116.318157
         * 5	8号公寓	40.157976	116.308231
         * 6	九号公寓	40.149332	116.304853
         * 7	鑫梦公寓	40.164775	116.303605
         */
        //housepos
        Point point1 = new Point(116.308994,40.159142);
        Point point2 = new Point(116.308429,40.159951);
        Point point3 = new Point(116.307872,40.159565);
        Point point4 = new Point(116.318157,40.158141);
        Point point5 = new Point(116.308231,40.157976);
        Point point6 = new Point(116.304853,40.149332);
        Point point7 = new Point(116.303605,40.164775);


        redisGeoUtil.addGeoPoint(SysConstant.HOUSE_GEO_PREFIX,point1,1);
        redisGeoUtil.addGeoPoint(SysConstant.HOUSE_GEO_PREFIX,point2,2);
        redisGeoUtil.addGeoPoint(SysConstant.HOUSE_GEO_PREFIX,point3,3);
        redisGeoUtil.addGeoPoint(SysConstant.HOUSE_GEO_PREFIX,point4,4);
        redisGeoUtil.addGeoPoint(SysConstant.HOUSE_GEO_PREFIX,point5,5);
        redisGeoUtil.addGeoPoint(SysConstant.HOUSE_GEO_PREFIX,point6,6);
        redisGeoUtil.addGeoPoint(SysConstant.HOUSE_GEO_PREFIX,point7,7);

        //
    }

    @Test
    public void get(){
        List<Point> points = redisGeoUtil.geoGet("housepos", "haixing");
    }
}
