//package com.rent;
//
//import com.alibaba.fastjson.JSONObject;
//import com.rent.beans.resp.HouseListResp;
//import com.rent.entity.House;
//import com.rent.service.HouseService;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.List;
//
///**
// * @author lihaoyang
// * @date 2020/11/12
// */
//@Slf4j
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class SampleTest {
//
////    @Autowired
////    RedisUtil redisUtil;
//
//    @Autowired
//    HouseService houseService;
//
//
//    public static void main(String[] args) {
//
//    }
//
//
//    @Test
//    public void insert() {
//
//        List<HouseListResp> list = houseService.nearbyHouse();
//        for(HouseListResp resp : list){
//            System.err.println(JSONObject.toJSONString(resp));
//        }
//    }
//
//    private static final String SHORT_URL_KEY = "SHORT_URL_KEY";
//    private static final String LOCALHOST = "http://localhost:4444/";
//    private static final String SHORT_LONG_PREFIX = "short_long_prefix_";
//    private static final String CACHE_KEY_PREFIX = "cache_key_prefix_";
//    private static final int CACHE_SECONDS = 1 * 60 * 60;
//
//
//
//
//    public String getShortURL(String longURL, Decimal decimal) {
//        // 查询缓存
//        String cache = (String) redisUtil.get(CACHE_KEY_PREFIX + longURL);
//        if (cache != null) {
//            return LOCALHOST + toOtherBaseString(Long.valueOf(cache), decimal.x);
//        }
//
//        // 自增
//        long num = redisUtil.incr(SHORT_URL_KEY,1);
//        // 在数据库中保存短-长URL的映射关系,可以保存在MySQL中
//        redisUtil.set(SHORT_LONG_PREFIX + num, longURL);
//        // 写入缓存
//        redisUtil.set(CACHE_KEY_PREFIX + longURL, CACHE_SECONDS, num);
//        return LOCALHOST + toOtherBaseString(num, decimal.x);
//    }
//
//    /**
//     * 在进制表示中的字符集合
//     */
//    final static char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
//            '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
//            'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y',
//            'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
//
//    /**
//     * 由10进制的数字转换到其他进制
//     */
//    private String toOtherBaseString(long n, int base) {
//        long num = 0;
//        if (n < 0) {
//            num = ((long) 2 * 0x7fffffff) + n + 2;
//        } else {
//            num = n;
//        }
//        char[] buf = new char[32];
//        int charPos = 32;
//        while ((num / base) > 0) {
//            buf[--charPos] = digits[(int) (num % base)];
//            num /= base;
//        }
//        buf[--charPos] = digits[(int) (num % base)];
//        return new String(buf, charPos, (32 - charPos));
//    }
//
//    enum Decimal {
//        D32(32),
//        D64(64);
//
//        int x;
//
//        Decimal(int x) {
//            this.x = x;
//        }
//    }
//
//
//}
