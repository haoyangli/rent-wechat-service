package com.rent;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.rent.beans.resp.HouseRegionResp;
import com.rent.constants.SysConstant;
import com.rent.entity.PublishRecord;
import com.rent.entity.WechatUser;
import com.rent.enums.DeleteEnum;
import com.rent.service.HouseService;
import com.rent.service.PublishRecordService;
import com.rent.service.WechatUserService;
import com.rent.utils.RedisGeoUtil;
import com.rent.utils.RentSecurityManager;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author lihaoyang
 * @date 2021/6/10
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class TTT1 {

    @Autowired
    private WechatUserService wechatUserService;
    @Autowired
    private HouseService houseService;
    @Autowired
    private PublishRecordService publishRecordService  ;

    @Test
    public void testNearByXY() {
        Integer userId = 3;
        //
        QueryWrapper<PublishRecord> queryWrapper = new QueryWrapper<>();
        queryWrapper
                .eq("user_id", userId)
                .eq("is_del", DeleteEnum.NO.getCode());
        List<PublishRecord> publishRecords = publishRecordService.list(queryWrapper);
        System.out.println(publishRecords);
//
//        WechatUser wechatUserService = this.wechatUserService.getByOpenId("oLkLm589SNiKBWEpwyeFF6soj6kk");
//        List<HouseRegionResp> houseRegionList = houseService.getHouseRegionList();
//        System.out.println(houseRegionList);
    }
}
